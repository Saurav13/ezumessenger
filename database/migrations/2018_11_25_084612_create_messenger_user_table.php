<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessengerUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('messenger_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('last_seen');
            $table->boolean('is_muted')->default(0);

            $table->foreign('messenger_id')->references('id')->on('messengers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_user');
    }
}
