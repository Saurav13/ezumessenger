<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $appends='user';
    public function getUserAttribute()
    {
        return User::findOrFail($this->id);
    }
    
}
