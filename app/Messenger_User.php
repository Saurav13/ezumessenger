<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messenger_User extends Model
{
    protected $table='messenger_user';
}
