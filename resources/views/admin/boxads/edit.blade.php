@extends('layouts.admin')

@section('body')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    
    #img-upload{
        height:30%;
        width: 30%;
        text-align: center;
        padding-top:10px;
        }
</style>
<link href="{{ asset('admin-assets/select2.css') }}" rel="stylesheet" />

    <div class="content-body">
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-minus4" aria-hidden="true"></i> Edit Box Ads</button></a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a href="{{ route('boxAds.index') }}"><i class="icon-arrow-left"></i></a></li>
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                            
                        <div class="card-body collapse in">
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('boxAds.update',$ad->id)}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="PATCH"/>
                                    <div class="row">
                                        <div class="form-body ">
                                            <div class="form-group col-sm-12">
                                                <label>Target</label>
                                                <div class="input-group">
                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                        <input type="radio" name="target" value="Teacher" {{ $ad->target == 'Teacher' ? 'checked' : '' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Teacher</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="radio" name="target" value="Student" {{ $ad->target == 'Student' ? 'checked' : '' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Student</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="radio" name="target" value="Both" {{ $ad->target == 'Both' ? 'checked' : '' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Both</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label for="selectfaculties">Faculty</label>
                                                <?php $cats = $ad->faculties()->pluck('faculties.id')->toArray(); ?>
                                                <select id="selectfaculties" class="form-control" name="faculty[]" class="js-example-basic-multiple" multiple="multiple" width="100%">
                                                    @foreach($faculties as $c)
                                                        <option value="{{ $c->id }}"  {{ in_array($c->id,$cats) ? 'selected' : '' }}>{{ $c->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="selectcolleges">College</label>
                                                <?php $colgs = $ad->colleges()->pluck('colleges.id')->toArray(); ?>
                                                <select id="selectcolleges" class="form-control" name="college[]" class="js-example-basic-multiple" multiple="multiple" width="100%">
                                                    @foreach($colleges as $c)
                                                        <option value="{{ $c->id }}"  {{ in_array($c->id,$colgs) ? 'selected' : '' }}>{{ $c->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label>Year</label>
                                                <div class="input-group">
                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                        <input type="checkbox" name="year[]" value="Current" {{ in_array('Current',json_decode($ad->year)) ? 'checked' : '' }} class="chk-remember">
                                                        <span class="custom-control-description ml-0">Current</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="checkbox" name="year[]" value="Pass Out" {{ in_array('Pass Out',json_decode($ad->year)) ? 'checked' : '' }} class="chk-remember">
                                                        <span class="custom-control-description ml-0">Pass Out</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label for="link">Link</label>
                                                <input type="text" id="link" class="form-control" placeholder="Link - http://www." name="link" value="{{ $ad->link }}" required>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="timesheetinput3">Expiry Date</label>
                                                <div class="position-relative has-icon-left">
                                                    <input type="date" id="timesheetinput3" class="form-control" value="{{ $ad->expiry_date }}" name="expiry_date" required>
                                                    <div class="form-control-position">
                                                        <i class="icon-calendar5"></i>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row col-sm-12">
                                                <div class="form-group">
                                                    <label for="projectinput8">Content</label>
                                                <textarea id="projectinput8" rows="5" class="form-control" name="content" placeholder="Ad content">{{$ad->content}}</textarea>
                                                </div>
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-md btn-success" id="add">
                                                    <i class="icon-check2">Save</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin-assets/select2.js') }}"></script>
   
    <script>
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });
    
            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }
    
            $("#imgInp").change(function(){
                readURL(this);
            });

            $('#selectfaculties').select2({
                placeholder: "Select faculties",
                width: '100%',
                height: '45px',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                
            });

            $('#selectcolleges').select2({
                placeholder: "Select College Names",
                width: '100%',
                height: '45px',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                
            }); 	
        });
    </script>


@endsection