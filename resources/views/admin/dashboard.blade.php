@extends('layouts.admin')

@section('body')
    <div class="content-header row">
    </div>
    <div class="content-body"><!-- stats -->
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="pink"></h3>
                                    <span>Today's Visit</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-eye5 pink font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="teal"></h3>
                                    <span>Total Users</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="deep-orange"> / 15 GB</h3>
                                    <span>Google Drive Space</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="cyan"></h3>
                                    <span>Total Visits</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-eye4 cyan font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="cyan"> GB</h3>
                                    <span>Used Drive Space</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-database2 cyan font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="deep-orange"> GB</h3>
                                    <span>Used Collection Space</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-database2 deep-orange font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="teal"> GB</h3>
                                    <span>Used Posts Space</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-database2 teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="pink"> GB</h3>
                                    <span>Assigned Users Space</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-database2 pink font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ stats -->
        <!--project Total Earning, visit & post-->
        <div class="row">
            
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <canvas id="posts-visits" class="height-400"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/project Total Earning, visit & post-->

        <div class="row match-height">
            <div class="col-xl-4 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 text-xs-center bg-deep-orange media-left media-middle">
                                <i class="icon-buysellads font-large-2 white"></i>
                            </div>
                            <div class="p-2 media-body">
                                <h5 class="deep-orange">Active Banner Ads</h5>
                                <h5 class="text-bold-400"></h5>
                                <progress class="progress progress-sm progress-deep-orange mt-1 mb-0" value=""></progress>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 text-xs-center bg-cyan media-left media-middle">
                                <i class="icon-buysellads font-large-2 white"></i>
                            </div>
                            <div class="p-2 media-body">
                                <h5>Active Box Ads</h5>
                                <h5 class="text-bold-400"></h5>
                                <progress class="progress progress-sm progress-cyan mt-1 mb-0" value=""></progress>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 media-body text-xs-left">
                                <h5>New Users</h5>
                                <h5 class="text-bold-400">1,22,356</h5>
                            </div>
                            <div class="p-2 text-xs-center bg-teal media-right media-middle">
                                <i class="icon-user1 font-large-2 white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Recent Invoices</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <p>Total paid invoices 240, unpaid 150. <span class="float-xs-right"><a href="#">Invoice Summary <i class="icon-arrow-right2"></i></a></span></p>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>Invoice#</th>
                                        <th>Customer Name</th>
                                        <th>Status</th>
                                        <th>Due</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-truncate"><a href="#">INV-001001</a></td>
                                        <td class="text-truncate">Elizabeth W.</td>
                                        <td class="text-truncate"><span class="tag tag-default tag-success">Paid</span></td>
                                        <td class="text-truncate">10/05/2016</td>
                                        <td class="text-truncate">$ 1200.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-truncate"><a href="#">INV-001012</a></td>
                                        <td class="text-truncate">Andrew D.</td>
                                        <td class="text-truncate"><span class="tag tag-default tag-success">Paid</span></td>
                                        <td class="text-truncate">20/07/2016</td>
                                        <td class="text-truncate">$ 152.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-truncate"><a href="#">INV-001401</a></td>
                                        <td class="text-truncate">Megan S.</td>
                                        <td class="text-truncate"><span class="tag tag-default tag-success">Paid</span></td>
                                        <td class="text-truncate">16/11/2016</td>
                                        <td class="text-truncate">$ 1450.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-truncate"><a href="#">INV-01112</a></td>
                                        <td class="text-truncate">Doris R.</td>
                                        <td class="text-truncate"><span class="tag tag-default tag-warning">Overdue</span></td>
                                        <td class="text-truncate">11/12/2016</td>
                                        <td class="text-truncate">$ 5685.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-truncate"><a href="#">INV-008101</a></td>
                                        <td class="text-truncate">Walter R.</td>
                                        <td class="text-truncate"><span class="tag tag-default tag-warning">Overdue</span></td>
                                        <td class="text-truncate">18/05/2016</td>
                                        <td class="text-truncate">$ 685.00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row match-height">
            
            <div class="col-xl-4 col-md-6 col-sm-12">
                <div class="card" style="height: 440px;">
                    <div class="card-body">
                        <div class="card-block">
                            <h4 class="card-title">Top  Categories</h4>
                            <p class="card-text">List of Top  categories and their class count. </p>
                        </div>
                        <ul class="list-group list-group-flush">
                           
                        </ul>
                        <div class="card-block">
                            <a href="" class="card-link">See All Categories</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-12 col-sm-12">
                <div class="card" style="height: 440px;">
                    <div class="card-body">
                        <div class="card-block">
                            <h4 class="card-title">Carousel</h4>
                            <h6 class="card-subtitle text-muted">Support card subtitle</h6>
                        </div>
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item">
                                    <img src="../../app-assets/images/carousel/02.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item active">
                                    <img src="../../app-assets/images/carousel/03.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img src="../../app-assets/images/carousel/01.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="icon-prev" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="icon-next" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="card-block">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    
    var ctx4 = $("#posts-visits");

    // Chart Options
    var PostsVisitsOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'top',
            labels: {
                boxWidth: 10,
                fontSize: 14
            },
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    lineWidth: 2,
                    color: "rgba(0, 0, 0, 0.05)",
                    zeroLineWidth: 2,
                    zeroLineColor: "rgba(0, 0, 0, 0.05)",
                    drawTicks: false,
                },
                ticks: {
                    fontSize: 14,
                }
            }],
            yAxes: [{
                display: false,
                ticks: {
                    min: 0,
                    max: 100
                }
            }]
        },
        title: {
            display: false,
            text: 'Chart.js Line Chart - Legend'
        }
    };
    // Chart Data
    var postsVisitsData = {
        labels: ['JAN','FEB','Mar'],
        datasets: [{
            label: "Visits",
            data: [1,2,3],
            lineTension: 0,
            fill: false,
            // borderDash: [5, 5],
            borderColor: "#37BC9B",
            pointBorderColor: "#37BC9B",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 3,
            pointRadius: 6,
        }]
    };

    var postsVisitsConfig = {
        type: 'line',

        // Chart Options
        options : PostsVisitsOptions,

        data : postsVisitsData
    };

    // Create the chart
    var postsVisitsChart = new Chart(ctx4, postsVisitsConfig);
</script>
@endsection