@extends('layouts.frontend')
@section('css')
<link rel="stylesheet" href="/frontend-assets/css/separate/pages/files.min.css">
<link rel="stylesheet" href="/frontend-assets/css/separate/pages/chat.min.css">
<link rel="stylesheet" href="/frontend-assets/css/separate/pages/messenger.min.css">
@stop
@section('body')
<style>
   .fm-file{
       width:unset;
   }
   .fm-file .fm-file-icon{
        height:50px;
        line-height:50px;
   }
   .fm-file .fm-file-icon img{
       vertical-align: middle;
        width:50px;
        height:50px;
   }
   .files-manager-content-in{
       margin-right:unset;
   }
</style>
<div class="page-content" style="padding-left:7rem">
    <div class="container-fluid messenger">

        <div class="box-typical chat-container">
            <section class="chat-list">
                <div class="chat-list-search chat-list-settings-header">
                        <input type="text" class="form-control form-control-rounded" placeholder="Search">
                </div><!--.chat-list-search-->
                <div class="chat-list-in scrollable-block" style="min-height:35rem">
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matt McGill</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">
                                <div class="icon">
                                    <i class="font-icon font-icon-pencil-thin"></i>
                                </div>
                                Matt McGill typing a message
                            </div>
                            <div class="chat-list-item-count">3</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matthew Heath</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">Anything that's easy or has no difficulty; something that is a certainty</div>
                            <div class="chat-list-item-count">100</div>
                        </div>
                    </div>
                    <div class="chat-list-item selected">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-3.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Vasilisa</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">no</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-4.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Administration</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">You can run!</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Monica Parrish</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">Monica Parrish changes the image on the page</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matt McGill</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">
                                <div class="icon">
                                    <i class="font-icon font-icon-pencil-thin"></i>
                                </div>
                                Matt McGill typing a message
                            </div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matt McGill</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">Anything that's easy or has no difficulty; something that is a certainty</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-3.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Vasilisa</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">no</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-4.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Administration</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">You can run!</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Monica Parrish</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">Monica Parrish changes the image on the page</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matt McGill</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">
                                <div class="icon">
                                    <i class="font-icon font-icon-pencil-thin"></i>
                                </div>
                                Matt McGill typing a message
                            </div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matthew Heath</span>
                            </div>
                            <div class="chat-list-item-date">16:59</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">Anything that's easy or has no difficulty; something that is a certaint</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-3.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Vasilisa</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">no</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                    <div class="chat-list-item online">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-4.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Administration</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt">You can run!</div>
                        </div>
                    </div>
                    <div class="chat-list-item">
                        <div class="chat-list-item-photo">
                            <img src="/frontend-assets/img/photo-64-1.jpg" alt="">
                        </div>
                        <div class="chat-list-item-header">
                            <div class="chat-list-item-name">
                                <span class="name">Matt McGill</span>
                            </div>
                            <div class="chat-list-item-date">05 Aug</div>
                        </div>
                        <div class="chat-list-item-cont">
                            <div class="chat-list-item-txt writing">Yes</div>
                            <div class="chat-list-item-dot"></div>
                        </div>
                    </div>
                </div><!--.chat-list-in-->
            </section><!--.chat-list-->

            <section class="chat-list-info">
                <div class="chat-list-search chat-list-settings-header">
                    <a href="#"><span class="fa fa-phone"></span></a>
                    <a href="#"><span class="fa fa-video-camera"></span></a>
                    <a href="#"><span class="fa fa-info-circle"></span></a>
                </div><!--.chat-list-search-->
                <div class="chat-list-in">
                  
                    <section class="chat-settings">
                        <div class="scrollable-block" style="height:10rem;">
                            <div class="chat-list-item">
                                <div class="chat-list-item-photo">
                                    <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                                </div>
                                <div class="chat-list-item-header" style="margin:0.5rem">
                                    <div class="chat-list-item-name">
                                        <span class="name" style="font-weight: 400;">Matthew Heath</span>
                                    </div>
                                </div>
                                <div class="chat-list-item-cont" style="padding:0">
                                    <span><i class="fa fa-remove" style="margin-top: -25px; float: right; color:red; cursor:pointer"></i></span>
                                </div>
                            </div>
                            <div class="chat-list-item">
                                <div class="chat-list-item-photo" >
                                    <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                                </div>
                                <div class="chat-list-item-header" style="margin:0.5rem">
                                    <div class="chat-list-item-name">
                                        <span class="name" style="font-weight: 400;">Matthew Heath</span>
                                    </div>
                                </div>
                                <div class="chat-list-item-cont" style="padding:0">
                                    <span><i class="fa fa-remove" style="margin-top: -25px; float: right; color:red; cursor:pointer"></i></span>
                                </div>
                            </div>
                            <div class="chat-list-item">
                                <div class="chat-list-item-photo">
                                    <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                                </div>
                                <div class="chat-list-item-header" style="margin:0.5rem">
                                    <div class="chat-list-item-name">
                                        <span class="name" style="font-weight: 400;">Matthew Heath</span>
                                    </div>
                                </div>
                                <div class="chat-list-item-cont" style="padding:0">
                                    <span><i class="fa fa-remove" style="margin-top: -25px; float: right; color:red; cursor:pointer"></i></span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="item">
                            <a href="#" data-toggle="modal" data-target="#addPeople">
                                <span class="font-icon font-icon-plus"></span>
                                Add People
                            </a>
                        </div>
                        
                        <div class="checkbox-toggle">
                            <input type="checkbox" id="check-toggle-2" checked="">
                            <label for="check-toggle-2">Mute</label>
                        </div>
                    </section>
                    <section class="chat-profiles">
                        <header>Shared Files</header>
                        <div class="files-manager-content">
                            <div class="files-manager-content-in scrollable-block" style="height:15rem">
                                <div class="fm-file-grid">
                                    <div class="fm-file selected">
                                        <div class="fm-file-icon">
                                            <img src="/frontend-assets/img/folder.png" alt="">
                                        </div>
                                        <div class="fm-file-name">App design</div>
                                    </div>
                                    <div class="fm-file">
                                        <div class="fm-file-icon">
                                            <img src="/frontend-assets/img/folder.png" alt="">
                                        </div>
                                        <div class="fm-file-name">Inspiration</div>
                                    </div>
                                    <div class="fm-file">
                                        <div class="fm-file-icon">
                                            <img src="/frontend-assets/img/folder.png" alt="">
                                        </div>
                                        <div class="fm-file-name">projects.rar</div>
                                    </div>
                                    <div class="fm-file">
                                        <div class="fm-file-icon">
                                            <img src="/frontend-assets/img/file.png" alt="">
                                        </div>
                                        <div class="fm-file-name">Inspiration</div>
                                    </div>
                                </div>
                            </div><!--.files-manager-content-in-->
                            <input type="text" class="form-control form-control-rounded" placeholder="Search Files..">
                            <br>
                        </div><!--.files-manager-content-->
                        
                        

                </div>
            </section>

            <section class="chat-area " >
                <div class="chat-area-in " >
                    <div class="chat-area-header">
                        <div class="chat-list-item online">
                            <div class="chat-list-item-name">
                                <span class="name">Thomas Bryan</span>
                            </div>
                            <div class="chat-list-item-txt writing">Last seen 05 aug 2015 at 18:04</div>
                        </div>
                    </div><!--.chat-area-header-->

                    <div class="chat-dialog-area scrollable-block" style="min-height:35rem">
                        <div class="messenger-dialog-area">
                            <div class="messenger-message-container">
                                <div class="avatar">
                                    <img src="/frontend-assets/img/avatar-1-32.png">
                                </div>
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="messenger-message-container from bg-blue">
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="avatar chat-list-item-photo">
                                    <img src="/frontend-assets/img/photo-64-1.jpg">
                                </div>
                            </div>
                            <div class="messenger-message-container">
                                <div class="avatar">
                                    <img src="/frontend-assets/img/avatar-1-32.png">
                                </div>
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="messenger-message-container from bg-blue">
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="avatar chat-list-item-photo">
                                    <img src="/frontend-assets/img/photo-64-1.jpg">
                                </div>
                            </div>
                            <div class="messenger-message-container">
                                <div class="avatar">
                                    <img src="/frontend-assets/img/avatar-1-32.png">
                                </div>
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                        <li>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                            <div class="time-ago">1:26</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="messenger-message-container from bg-blue">
                                <div class="messages">
                                    <ul>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="time-ago">1:26</div>
                                            <div class="message">
                                                <div>
                                                    Lorem Ipsum is simply dummy text...
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="avatar chat-list-item-photo">
                                    <img src="/frontend-assets/img/photo-64-1.jpg">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="chat-area-bottom">
                        <form class="write-message">
                            <div class="form-group">
                                <textarea rows="1" class="form-control" placeholder="Type a message"></textarea>
                                <div class="dropdown dropdown-typical dropup attach">
                                    <a class="dropdown-toggle dropdown-toggle-txt"
                                       id="dd-chat-attach"
                                       data-target="#"
                                       data-toggle="dropdown"
                                       aria-haspopup="true"
                                       aria-expanded="false">
                                        <span class="font-icon fa fa-file-o"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-chat-attach">
                                        <a class="dropdown-item" href="#"><i class="font-icon font-icon-cam-photo"></i>Photo</a>
                                        <a class="dropdown-item" href="#"><i class="font-icon font-icon-cam-video"></i>Video</a>
                                        <a class="dropdown-item" href="#"><i class="font-icon font-icon-sound"></i>Audio</a>
                                        <a class="dropdown-item" href="#"><i class="font-icon font-icon-page"></i>Document</a>
                                        <a class="dropdown-item" href="#"><i class="font-icon font-icon-earth"></i>Map</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!--.chat-area-bottom-->
                </div><!--.chat-area-in-->
            </section><!--.chat-area-->
        </div><!--.chat-container-->

    
    </div><!--.container-fluid-->
</div><!--.page-content-->
<div class="modal fade" id="addPeople" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                <i class="font-icon-close-2"></i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add People</h4>
        </div>
        <div class="modal-body">
            <div class="chat-list-search chat-list-settings-header">
                <input type="text" class="form-control form-control-rounded" placeholder="Search">
            </div>
            <div class="scrollable-block" style="height:20rem;">
                <div class="chat-list-item">
                    <div class="chat-list-item-photo" style="height:45px; width:45px;">
                        <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                    </div>
                    <div class="chat-list-item-header" style="margin:1rem">
                        <div class="chat-list-item-name">
                            <span class="name" style="font-weight: 400;font-size: 20px;">Matthew Heath</span>
                        </div>
                        <div class="chat-list-item-cont" style="padding:0">
                            <input type="checkbox" class="" style="    margin-top: -18px; float: right;"/>
                        </div>
                    </div>
                </div>
                <div class="chat-list-item">
                    <div class="chat-list-item-photo" style="height:45px; width:45px;">
                        <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                    </div>
                    <div class="chat-list-item-header" style="margin:1rem">
                        <div class="chat-list-item-name">
                            <span class="name" style="font-weight: 400;font-size: 20px;">Matthew Heath</span>
                        </div>
                        <div class="chat-list-item-cont" style="padding:0">
                            <input type="checkbox" class="" style="    margin-top: -18px; float: right;"/>
                        </div>
                    </div>
                </div>
                <div class="chat-list-item">
                    <div class="chat-list-item-photo" style="height:45px; width:45px;">
                        <img src="/frontend-assets/img/photo-64-2.jpg" alt="">
                    </div>
                    <div class="chat-list-item-header" style="margin:1rem">
                        <div class="chat-list-item-name">
                            <span class="name" style="font-weight: 400;font-size: 20px;">Matthew Heath</span>
                        </div>
                        <div class="chat-list-item-cont" style="padding:0">
                            <input type="checkbox" class="" style="    margin-top: -18px; float: right;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-rounded btn-primary">Add</button>
        </div>
    </div>
    </div>
</div><!--.modal-->

@endsection