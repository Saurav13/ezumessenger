@include('frontend.partials.header')
@include('frontend.partials.nav')
@include('frontend.partials.messages')
@yield('body')
@include('frontend.partials.footer')

