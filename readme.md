

## Changes
In Ezumaster\vendor\laravel\framework\src\Illuminate\Auth\SessionGuard, replace 'userFromRecaller' function with this.
( Also, include 
				use App\User;
				use App\MyBroker; 
)

	protected function userFromRecaller($recaller)
    {
        if (! $recaller->valid() || $this->recallAttempted) {
            return;
        }

        // If the user is null, but we decrypt a "recaller" cookie we can attempt to
        // pull the user data on that cookie which serves as a remember cookie on
        // the application. Once we have a user we can return it to the caller.
        $this->recallAttempted = true;

        $this->viaRemember = ! is_null($user = $this->provider->retrieveByToken(
            $recaller->id(), $recaller->token()
        ));

        $broker = new MyBroker;
        $result = $broker->checklogin($recaller->id(),$recaller->token());

        if(!User::where('email',$result)->exists()){
            $user= User::create([
                'email' => $result,
            ]);
        }
        else
            $user = User::where('email',$result)->first();

        return $user;
    }

-----------------------------------------------------------------------------------------------------------------------------

.env file

APP_NAME=Laravel  
APP_ENV=local  
APP_KEY=base64:SgAEZfHWhEfRh8riKau7C4zHfqdudFyF6RWZeNNuNqs=  
APP_DEBUG=true  
APP_URL=http://localhost  

LOG_CHANNEL=stack  

DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=ezumaster  
DB_USERNAME=root  
DB_PASSWORD=  

DB_CONNECTION_SECOND=mysql  
DB_HOST_SECOND=127.0.0.1  
DB_PORT_SECOND=3306  
DB_DATABASE_SECOND=ezuaccount  
DB_USERNAME_SECOND=root  
DB_PASSWORD_SECOND=  

BROADCAST_DRIVER=log  
CACHE_DRIVER=file  
SESSION_DRIVER=file  
SESSION_LIFETIME=120  
QUEUE_DRIVER=sync  

REDIS_HOST=127.0.0.1  
REDIS_PASSWORD=null  
REDIS_PORT=6379  

MAIL_DRIVER=smtp  
MAIL_HOST=smtp.mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=7735e54d2218b8  
MAIL_PASSWORD=744e9b247c6ab5  
MAIL_ENCRYPTION=null  

PUSHER_APP_ID=  
PUSHER_APP_KEY=  
PUSHER_APP_SECRET=  
PUSHER_APP_CLUSTER=mt1  

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"  
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"  

SSO_SERVER_URL=http://localhost:8000/server/  
SSO_BROKER_ID=EzuMaster  
SSO_BROKER_SECRET=dYXnAwzYz3  

ACCOUNT_URL=http://localhost:8000/  
-----------------------------------------------------------------------------------------------------------------------------